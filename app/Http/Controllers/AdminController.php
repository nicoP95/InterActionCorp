<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use \App\User;
use \App\ambienti;
use Auth;


class AdminController extends Controller
{		

 public function __construct(){
  $this->middleware('auth');
}


public function AutHandle(){

  $Sens = \App\Sensori::all();
  $user = Auth::user();
  return view('/userviews/authandle', compact('Sens','user'));
}

public function UserHandle(){


  $all_user=DB::table('Users')
  ->where('accept','=', 1)
  ->get();
  $user = Auth::user();

  $flag=0;
  //dd(view('/userviews/user_handle', compact('all_user','user','flag')));
  return view('/userviews/user_handle', compact('all_user','user','flag'));
}


public function AddUser(){
  $all_user =DB::table('Users')
  ->where('accept','=', 0)
  ->get();
  //dd(view('/userviews/adminpage',compact('all_user')))  ;  
  return view('/userviews/adminpage',compact('all_user'));
}


  public function Accept($user_id){
  $accept_user = $this->getUser($user_id);
  $accept_user->accept = 1;
  $accept_user->save();
  return redirect()->action( 'AdminController@AddUser');
  }

  public function getUser($user_id){
    $user=\App\User::find($user_id);
    return $user;
  }
  public function checkIfUserAccept($user_id){
    $user=$this->getUser($user_id);
    if($user->accept==0){
      $user_accept=false;
    }else{
      $user_accept=true;
    }
    return $user_accept;
  }
}
