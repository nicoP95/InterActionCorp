<?php
namespace Tests;
use Modelizer\Selenium\SeleniumTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class SeleniumExampleTest extends SeleniumTestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */

    public function setUp()
    {
        $this->setHost('127.0.0.1');
        $this->setPort(4444);
        $this->setBrowser('chrome');
        $this->setBrowserUrl('http://127.0.0.1');

    }


    public function testBasicExample()
    {
        // This is a sample code you can change as per your current scenario
        $this->visit('/home')
             ->see('home')
             ->hold(3);
    }

    /**
     * A basic submission test example.
     *
     * @return void
     */
    public function testLoginFormExample()
    {
          
 	    $this->assertContains(4, [1, 2, 3,4]);
    }
}