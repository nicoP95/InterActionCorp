<?php

namespace Tests;
use Modelizer\Selenium\SeleniumTestCase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
